<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
        <title>ログイン</title>
    </head>
    <body>
    	<div class="header">
			<div class="title">
				掲示板
			</div>
		</div>
   		<c:if test="${ not empty errorMessages }">
			<div class="error-message">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
        <div class="back">
	        <div class="main-content">
	        <div class="heading">
					ログイン
				</div>
	            <form action="login" method="post">
	            	<ul>
	            		<li>
			                <label for="account">アカウントID</label>
			                <input name="account" value ="${ account }" id="account"/>
			             </li>
			             <li>
			                <label for="password">パスワード</label>
			                <input name="password" type="password" id="password"/>
			             </li>
			             <li class="submit">
			                <input type="submit" value="ログイン" />
			              </li>
	                </ul>
	            </form>
	       </div>
       </div>
       <div class="footer">Copyright(c)Fumiya Ito</div>
    </body>
</html>