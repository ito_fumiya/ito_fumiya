<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
    	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<title>掲示板トップ</title>
	</head>
	<body>
		<div class="header clearfix">
			<div class="title">
				掲示板
			</div>
			<div class="head-link">
				<a class="link-btn" href="management" style="text-decoration: none; color:black;">管理画面</a>
				<a class="link-btn" href="newpost" style="text-decoration: none; color:black;">新規投稿</a>
				<a class="link-btn" href="logout" style="text-decoration: none; color:black;">ログアウト</a>
			</div>
		</div>


		<div class="back">
			<c:if test="${ not empty errorMessages }">
				<div class="error-message">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>
			<c:if test="${ not empty successMessage }">
				<div class="success-message">
					<c:out value="${successMessage}" />
				</div>
			</c:if>
			<c:remove var="errorMessages" scope="session" />
			<c:remove var="successMessage" scope="session" />
			<div class="search-box">
			<form action="./" method="get">日付
				<input type="date" name="start" value="${ start }" /> ～
				<input type="date" name="end" value="${ end }" /><br />
				カテゴリ<input name="category" value="${ category }" /><br />
				<input type="submit" value="検索" />
			</form>
			</div>
			<c:if test="${ not empty start || not empty end || not empty category }">
			<div class="search-message">
				<c:if test="${ not empty start }" ><c:out value="${start}" />～<br /></c:if>
				<c:if test="${ not empty end }" >～<c:out value="${end}" /><br /></c:if>
				<c:if test="${ not empty category }" ><c:out value="${category}" /><br /></c:if>
					の検索結果 <c:out value="${posts.size()}" /> 件
			</div>
			</c:if>
			<c:if test="${ not empty posts }">
			<div class="main-content">
				<c:forEach items="${ posts }" var="post" >
					<div class="post-box">

						<div class="post-title">
							<c:out value="${post.title}" />
							<div class="delete-btn">
								<c:if test="${ post.userId == loginUser.id}">
									<form action="deletePost" method="post">
										<input type="hidden" name="postId" value="${post.id}">
					          			<input type="submit" value="投稿削除" onClick="return deleteBtn()">
			        				</form>
			       				</c:if>
		       				</div>
						</div>
						<div class="category">
							カテゴリ：<c:out value="${post.category}" />
						</div>
						<div class="category-line"></div>
						<div class="posts">
							<div class="post-content">
								<c:out value="${post.text}" />
							</div>
							<div class="post-data1">
								@<c:out value="${post.name}" />
								<div class="post-date"><fmt:formatDate value="${post.createdAt}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
							</div>
						</div>
						<div class="comment-box">
							<c:forEach items="${ comments }" var="comment">
								<c:if test="${ post.id == comment.postId }">
									<div class="comments clearfix">
										<div class="comment-content">
											<c:out value="${ comment.text }" />
										</div>
										<div class="post-data2">
											@<c:out value="${ comment.name }" /><br />
											<fmt:formatDate value="${comment.createdAt}" pattern="yyyy/MM/dd HH:mm:ss" />
										</div>
										<div class="delete-btn">
											<c:if test="${ comment.userId == loginUser.id }">
												<form action="deleteComment" method="post">
													<input type="hidden" name="commentId" value="${comment.id}">
						          					<input type="submit" value="コメント削除" onClick="return deleteBtn()">
					       						</form>
					       					</c:if>
				       					</div>
			       					</div>
								</c:if>
							</c:forEach>
						</div>
						<div class="comment-form">
							<form action="comment" method="post">
								<input type="hidden" name="postId" value="${post.id}">
									<textarea name="text" cols="50" rows="3" class="comment-box"></textarea><br />
			          			<input type="submit" value="コメントする">
		       				</form>
						</div>

					</div>
				</c:forEach>
			</div>
			</c:if>
		</div>
		<div class="footer">Copyright(c)Fumiya Ito</div>
	</body>


	<script type="text/javascript">

		//削除ダイアログ
	      function deleteBtn(){
	        if(window.confirm('削除してよろしいですか？')){
	          location.href = "/ito_fumiya/";
	          return true;
	        }
	        return false;
	      }

	      var dist = 30;
	      $(window).scroll(function() {
	        if ($(window).scrollTop() > 30) {
	          $('.header').addClass('scroll-header');
	          $('.title').addClass('scroll-header');
	          $('.head-link').addClass('scroll-header');
	        } else {
	          $('.header').removeClass('scroll-header');
	          $('.title').removeClass('scroll-header');
	          $('.head-link').removeClass('scroll-header');
	        }
	      });
    </script>

</html>

