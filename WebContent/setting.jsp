<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
        <title>ユーザー編集</title>
    </head>
    <body>
	    <div class="header clearfix">
			<div class="title">
				<a href="./" style="text-decoration: none; color:black;">掲示板</a>
			</div>
			<div class="head-link">
				<a class="link-btn" href="management" style="text-decoration: none; color:black;">管理画面</a>
				<a class="link-btn" href="./" style="text-decoration: none; color:black;">Home</a>
			</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="error-message">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<div class="back">
			<div class="main-content">
			<div class="heading">
					ユーザー情報編集
				</div>
            <form action="setting" method="post"><br />
                <input name="user_id" value="${editUser.id}" id="id" type="hidden"/>
            	 <ul>
	            	<li>
		                <label for="name">名前</label>
		                <input name="name" value="${editUser.name}" id="name"/>
		            </li>
		            <li>
		                <label for="account">アカウント名</label>
		                <input name="account" value="${editUser.account}" id="account"/>
		            </li>

                <c:if test="${ loginUser.id == editUser.id}">
                	<input name ="branch_id" value="${ editUser.branchId }" type="hidden">
                	<input name ="position_id" value="${ editUser.positionId }" type="hidden">
                </c:if>

                <c:if test="${ loginUser.id != editUser.id}">
                	<li>
		                <label for="name">支店</label>
		                <select name="branch_id">
		                <c:forEach items="${ branches }" var="branch">
			               <c:if test="${ editUser.branchId == branch.id }">
			               		<option selected value="${ branch.id }">
			               			<c:out value="${ branch.name}" />
			               		</option>
			               </c:if>
			               <c:if test="${ editUser.branchId != branch.id }">
			               		<option value="${ branch.id }">
			               			<c:out value="${ branch.name}" />
			               		</option>
			               </c:if>
			            </c:forEach>
		                </select>
		             </li>
		             <li>
		                <label for="name">役職</label>
		                <select name="position_id">
		                	<c:forEach items="${ positions }" var="position">
		                		<c:if test="${ editUser.positionId == position.id }">
		                			<option selected value="${ position.id }">
		                				<c:out value="${ position.name}" />
		                			</option>
		                		</c:if>
		                		<c:if test="${ editUser.positionId != position.id }">
		                			<option value="${ position.id }">
		                				<c:out value="${ position.name}" />
		                			</option>
		                		</c:if>
		                	</c:forEach>
		                </select>
		              </li>
		            </c:if>
		            <li>
		                <label for="password">パスワード</label>
		                <input name="password" type="password" id="password"/>
		           </li>
		           <li>
		                <label for="password">パスワード確認</label>
		                <input name="confirmationPassword" type="password" id="confirmationPassword"/>
		            </li>
		            <li class="submit">
	                	<input type="submit" value="編集する" />
	               	</li>
                </ul>
            </form>
        </div>
        </div>
        <div class="footer">Copyright(c)Fumiya Ito</div>
    </body>
</html>