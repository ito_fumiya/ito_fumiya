<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript">
	      function stop(){
	        if(window.confirm('停止してよろしいですか？')){
	          location.href = "/ito_fumiya/management";
	          return true;
	        }
	        return false;
	      }
	      function revival(){
	        if(window.confirm('復活してよろしいですか？')){
	          location.href = "/ito_fumiya/management";
	          return true;
	        }
	        return false;
	      }
    </script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
		<title>ユーザー管理画面</title>
	</head>

	<body>
		<div class="header clearfix">
			<div class="title">
				<a href="./" style="text-decoration: none; color:black;">掲示板</a>
			</div>
			<div class="head-link">
				<a class="link-btn" href="./" style="text-decoration: none; color:black;">Home</a>
				<a class="link-btn" href="signup" style="text-decoration: none; color:black;">ユーザ新規登録</a>
			</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="error-message">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<c:if test="${ not empty successMessage }">
			<div class="success-message">
				<c:out value="${successMessage}" />
			</div>
		</c:if>
		<c:remove var="errorMessages" scope="session" />
		<c:remove var="successMessage" scope="session" />
		<div class="back">
			<div class="main-content-wide">
				<div class="heading">
					ユーザー管理
				</div>
					<div class="table">
						<table border="0">
							<tr>
								<th>名前</th>
								<th>アカウントID</th>
								<th>支店</th>
								<th>部署</th>
								<th></th>
								<th></th>
							</tr>
							<c:forEach items="${ users }" var="user">
							<tr>
								<td><c:out value="${ user.name }" /></td>
								<td><c:out value="${ user.account }" /></td>
								<td><c:out value="${ user.branchName }" /></td>
								<td><c:out value="${ user.positionName }" /></td>
								<td><a class="link-btn2" href="setting?user_id=${user.id}" style="text-decoration: none; color:black;">編集</a>
								<td>
									<form action="settingStates" method="post">
									<c:if test="${ user.id != loginUser.id }">
										<c:choose>
											<c:when test="${user.states == 1}">
												<input name="states" value="0" id="states" type="hidden"/>
												<input name="userId" value="${ user.id }" id="userId" type="hidden"/>
												<input type="submit" value="停止させる" onClick="return stop()"/>
											</c:when>
											<c:otherwise>
												<input name="states" value="1" id="states" type="hidden"/>
												<input name="userId" value="${ user.id }" id="userId" type="hidden"/>
											x<input type="submit" value="復活させる" onClick="return revival()"/>
											</c:otherwise>
										</c:choose>
									</c:if>
									</form>
								</td>

							</tr>
							</c:forEach>
						</table>
					</div>
			</div>
		</div>
		<div class="footer">Copyright(c)Fumiya Ito</div>
	</body>
</html>
