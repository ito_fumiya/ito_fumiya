<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
    <title>ユーザー登録</title>
    </head>
    <body>

    	<div class="header clearfix">
			<div class="title">
				<a href="./" style="text-decoration: none; color:black;">掲示板</a>
			</div>
			<div class="head-link">
				<a class="link-btn" href="./" style="text-decoration: none; color:black;">Home</a>
				<a class="link-btn" href="./management" style="text-decoration: none; color:black;">管理画面へ</a>
			</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="error-message">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
		<div class="back">
			<div class="main-content">
			<div class="heading">
					ユーザー新規登録
				</div>
	            <form action="signup" method="post">
	            <ul>
	            	<li>
		            	<label for="name">名前</label>
		            	<input name="name" value="${user.name}" id="name" />
	            	</li>
	            	<li>
		                <label for="account">アカウントID</label>
		                <input name="account"  value="${user.account}" id="account" />
		            </li>
		            <li>
		                <label for="branch_id">支店</label>
		                <select name="branch_id" id="branch_id">
		                	<c:forEach items="${ branches }" var="branch">
		                		<c:if test="${ user.branchId == branch.id }">
				               		<option selected value="${ branch.id }">
				               			<c:out value="${ branch.name}" />
				               		</option>
				               </c:if>
				               <c:if test="${ user.branchId != branch.id }">
				               		<option value="${ branch.id }">
				               			<c:out value="${ branch.name}" />
				               		</option>
				               </c:if>
		                	</c:forEach>
		                </select><br />
		            </li>
		            <li>
		                <label for="position_id">役職</label>
		                <select name="position_id">
		                	<c:forEach items="${ positions }" var="position">
		                		<c:if test="${ user.positionId == position.id }">
			                			<option selected value="${ position.id }">
			                				<c:out value="${ position.name}" />
			                			</option>
			                		</c:if>
			                		<c:if test="${ user.positionId != position.id }">
			                			<option value="${ position.id }">
			                				<c:out value="${ position.name}" />
			                			</option>
			                		</c:if>
		                	</c:forEach>
		                </select><br />
	                </li>
	                <li>
		                <label for="password">パスワード</label>
		                <input name="password" type="password" id="password" />
		            </li>
		            <li>
		                <label for="password">パスワード（確認用）</label>
		                <input name="confirmationPassword" type="password" id="confirmationPassword" />
		            </li>
		            <li class="submit">
		                <input name="states" id="states" type="hidden" value="1" />
		                <input type="submit" value="登録" /><br />
	                </li>
	            </ul>
	            </form>
            </div>
         </div>
         <div class="footer">Copyright(c)Fumiya Ito</div>

    </body>
</html>