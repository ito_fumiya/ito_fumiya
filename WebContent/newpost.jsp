<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/style.css">
		<title>新規投稿</title>
	</head>
	<body>
		<div class="header clearfix">
			<div class="title">
				<a href="./" style="text-decoration: none; color:black;">掲示板</a>
			</div>
			<div class="head-link">
				<a class="link-btn" href="./" style="text-decoration: none; color:black;">Home</a>
			</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="error-message">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<c:remove var="errorMessages" scope="session"/>
		<div class="back">
			<div class="main-content-wide">
				<div class="heading">
					新規投稿
				</div>
				<div class="submit">
					<form action="newpost" method="post">
						カテゴリ<br />
		            	<input name="category" value="${ post.category }" id="category" /><br />

			            タイトル<br />
		            	<input name="title" value="${ post.title }" id="title" /><br />

		            	本文<br />
			            <textarea name="text" cols="80" rows="5" class="post-text">${ post.text }</textarea>
			            <br />
			            <input type="submit" value="投稿する"><br />（1000文字まで）
			        </form>
			     </div>
			</div>
		</div>
		<div class="footer">Copyright(c)Fumiya Ito</div>
	</body>
</html>