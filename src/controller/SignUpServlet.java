package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<Branch> branches = new BranchService().getBranch();
    	List<Position> positions = new PositionService().getPosition();


        request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();
        boolean settingPassword = true;

        User user = new User();
        user.setName(request.getParameter("name"));
        user.setAccount(request.getParameter("account"));
        user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
        user.setPositionId(Integer.parseInt(request.getParameter("position_id")));
        user.setStates(Integer.parseInt(request.getParameter("states")));
        user.setPassword(request.getParameter("password"));

        //アカウントチェック
        if( UserService.isIdValid(request) == true) {
        	messages.add("そのアカウントIDはすでに使用されています");
        }
        //ユーザー情報チェック
        if(UserService.isValid(request, messages,settingPassword) == true) {
            new UserService().register(user);

            HttpSession session = request.getSession();

            String message = "ユーザーを登録しました";
            session.setAttribute("successMessage", message);

            response.sendRedirect("./management");

        } else {
        	List<Branch> branches = new BranchService().getBranch();
         	List<Position> positions = new PositionService().getPosition();

        	request.setAttribute("errorMessages", messages);
            request.setAttribute("user", user);
            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);

            request.getRequestDispatcher("signup.jsp").forward(request, response);

        }
    }

}