package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> Messages = new ArrayList<String>();
        System.out.println("CommentServlet in");

        User user = (User) session.getAttribute("loginUser");

        Comment comment = new Comment();
        comment.setText(request.getParameter("text"));
        comment.setUserId(user.getId());
        comment.setPostId(Integer.parseInt(request.getParameter("postId")));

        if (CommentService.isCommentValid(request, Messages) == true) {

            new CommentService().register(comment);

            String message = "コメントを投稿しました";
            session.setAttribute("successMessage", message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", Messages);

            response.sendRedirect("./");
        }
    }

}
