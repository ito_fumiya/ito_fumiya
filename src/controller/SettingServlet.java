package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        int userId = Integer.parseInt(request.getParameter("user_id"));

        User editUser = new UserService().getUser(userId);

        List<Branch> branches = new BranchService().getBranch();
    	List<Position> positions = new PositionService().getPosition();

        request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);
        request.setAttribute("editUser", editUser);

        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);
        boolean settingPassword = false;

        //同一アカウントIDを通す＆重複チェック
        if(UserService.accountThrough(request) == false) {
        	if( UserService.isIdValid(request) == true) {
            	messages.add("そのアカウント名はすでに使用されています");
            }
        }

        //ユーザー情報バリデーション
        if (UserService.isValid(request, messages,settingPassword) == true) {
            try {
                new UserService().update(editUser);

            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                request.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("settings.jsp").forward(request, response);
                return;
            }
            User loginUser = (User) session.getAttribute("loginUser");

            if(loginUser.getId() == editUser.getId()) {
            	session.setAttribute("loginUser", editUser);
            }
            response.sendRedirect("./management");
        } else {
        	List<Branch> branches = new BranchService().getBranch();
         	List<Position> positions = new PositionService().getPosition();

            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);
            request.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);

            request.getRequestDispatcher("setting.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {
        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("user_id")));
        editUser.setName(request.getParameter("name"));
        editUser.setAccount(request.getParameter("account"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
        editUser.setPositionId(Integer.parseInt(request.getParameter("position_id")));
        return editUser;
    }


}