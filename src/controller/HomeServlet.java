package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserPost;
import service.CommentService;
import service.PostService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	request.setCharacterEncoding("UTF-8");

    	String start =  request.getParameter("start");
    	String end = request.getParameter("end");
    	String category = request.getParameter("category");

    	List<UserPost> posts = new PostService().getPost(start, end, category);

    	request.setAttribute("start",start);
    	request.setAttribute("end",end);
    	request.setAttribute("category",category);

    	request.setAttribute("posts", posts);

    	List<UserComment> comments = new CommentService().getComment();
    	request.setAttribute("comments", comments);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }


}