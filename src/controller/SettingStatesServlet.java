package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;

@WebServlet(urlPatterns = { "/settingStates" })
public class SettingStatesServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("management.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
          HttpServletResponse response) throws ServletException, IOException {
    	System.out.println("SettingStatesServlet in");
    	int userId = Integer.parseInt(request.getParameter("userId"));
    	int userStates = Integer.parseInt(request.getParameter("states"));

        new UserService().updateStates(userId,userStates);

        System.out.println("SettingStatesServlet out");
        response.sendRedirect("./management");
    }

}