package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	System.out.println("Login Servlet in");

        String account = request.getParameter("account");
        String password = request.getParameter("password");

        UserService userService = new UserService();
        User user = userService.login(account, password);

        HttpSession session = request.getSession();
        if (user != null && user.getStates() == 1) {

            session.setAttribute("loginUser", user);
            response.sendRedirect("./");
        } else {

            List<String> messages = new ArrayList<String>();

            if(StringUtils.isBlank(account) == true && StringUtils.isBlank(password) == true) {
            	messages.add("アカウントとパスワードを入力してください");
            } else {
            	messages.add("ログインに失敗しました");
            }

            session.setAttribute("errorMessages", messages);
            request.setAttribute("account", account);
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }

}