package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Post;
import beans.User;
import service.PostService;
@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("newpost.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	System.out.println("NewPostServlet doPost");

        HttpSession session = request.getSession();

        List<String> Messages = new ArrayList<String>();

        User user = (User) session.getAttribute("loginUser");

        Post post = new Post();
        post.setTitle(request.getParameter("title"));
        post.setText(request.getParameter("text"));
        post.setCategory(request.getParameter("category"));
        post.setUserId(user.getId());

        if (PostService.isPostValid(request, Messages) == true) {

            new PostService().register(post);

            String message = "投稿しました";
            session.setAttribute("successMessage", message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", Messages);
            request.setAttribute("post", post);
            request.getRequestDispatcher("newpost.jsp").forward(request, response);
        }
    }

}