package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;

public class CommentService {

	//コメント投稿
    public void register(Comment Comment) {
        Connection connection = null;
        try {
        	System.out.println("CommentService register() IN");
            connection = getConnection();

            CommentDao CommentDao = new CommentDao();
            CommentDao.insert(connection, Comment);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //コメント表示
    public List<UserComment> getComment() {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            List<UserComment> ret = commentDao.getUserComments(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //コメント削除
    public void deleteComment(int CommentId) {
        Connection connection = null;
        try {
        	System.out.println("CommentService deleteComment() IN");
            connection = getConnection();

            CommentDao CommentDao = new CommentDao();
            CommentDao.delete(connection, CommentId);

            commit(connection);
            System.out.println("CommentService deleteComment() out");
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //コメントバリデーション
    public static boolean isCommentValid(HttpServletRequest request, List<String> Messages) {

        String message = request.getParameter("text");

        if (StringUtils.isBlank(message)) {
            Messages.add("空のコメントをすることはできません");
        } else if (500 < message.length()) {
            Messages.add("500文字以下で入力してください");
        }
        if (Messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}