package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

	//ユーザ登録
    public void register(User user) {
        Connection connection = null;
        try {
        	 System.out.println(" UserService register ");
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    //ログイン
    public User login(String account, String password) {

        Connection connection = null;
        try {
        	 System.out.println(" UserService login()");
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, account, encPassword);

            commit(connection);


            return user;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }



    //ユーザー編集
    public User getUser(int userId) {

        Connection connection = null;
        try {

        	System.out.println(" UserService getUser");
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.getUser(connection, userId);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            //パスワードが空だったら暗号化しない
            if(StringUtils.isBlank(user.getPassword()) == false ){
	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    //ユーザ一覧取得
    public List<User> getUserIndex() {
        Connection connection = null;
        try {
        	System.out.println(" UserService getUserIndex in");
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<User> user = userDao.getUserIndex(connection);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ユーザーステータス変更
    public void updateStates(int userId,int userStates) {

        Connection connection = null;
        try {
            connection = getConnection();
            System.out.println(" UserService updateStates");

            UserDao userDao = new UserDao();
            userDao.updateStates(connection, userId, userStates);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

  //ユーザー情報のバリデーション
    public static boolean isValid(HttpServletRequest request, List<String> messages, boolean settingPassword) {

    	System.out.println(" UserService isValid");

        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String confirmationPassword = request.getParameter("confirmationPassword");
        String name = request.getParameter("name");
        int branchId = (Integer.parseInt(request.getParameter("branch_id")));
        int positionId = (Integer.parseInt(request.getParameter("position_id")));

        if(StringUtils.isBlank(name) == true ) {
            messages.add("名前を入力してください");
        }else if(10 < name.length()) {
        	messages.add("名前は10文字以下で入力してください");
        }

        //アカウントフォーマットチェック
        if(StringUtils.isBlank(account) == true ) {
            messages.add("アカウントIDを入力してください");
        }else if(account.matches("[^0-9a-zA-Z]+") || (6 > account.length() || 20 < account.length())) {
        	messages.add("アカウントIDは英数字で6～20文字で入力してください");
        }

        //支店と役職の組み合わせ  優先させたい条件を（）で囲む
        if(branchId != 1 && (positionId == 1 || positionId == 2)) {
        	messages.add("支店と役職の組み合わせが不正です");
        }
        if(branchId == 1 && (positionId == 3 || positionId == 4)) {
        	messages.add("支店と役職の組み合わせが不正です");
        }


        //サインアップのパスワードフォーマットチェック
        if(settingPassword == true) {
	        if(StringUtils.isBlank(password) == true) {
	            messages.add("パスワードを入力してください");
	        }else if(account.matches("[^ -/:-@\\[-\\`\\{-\\~0-9a-zA-Z]+") || 6 > password.length() || 20 < password.length()) {
	        	messages.add("パスワードは英数字と記号を含む6～20文字で入力してください");
	        }else if(!password.equals(confirmationPassword)) {
	        	messages.add("パスワードと確認用パスワードが一致していません");
	        }
        }

        //セッティング時にパスワード入力があったら
        if(settingPassword == false && StringUtils.isBlank(password) == false) {
        	if(account.matches("[^ -/:-@\\[-\\`\\{-\\~0-9a-zA-Z]+") || 6 > password.length() || 20 < password.length()) {
	        	messages.add("パスワードは英数字と記号を含む6～20文字で入力してください");
        	}else if(!password.equals(confirmationPassword)) {
	        	messages.add("パスワードと確認用パスワードが一致していません");
	        }
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }


    //アカウントID重複チェック
    public static boolean isIdValid(HttpServletRequest request) {

    	System.out.println(" UserService isIdValid");

        String account = request.getParameter("account");

        List<User> users = new UserService().getUserIndex();
        Set<String> set = new HashSet<>();

        for(int i=0; i <users.size() ; i++){
        	String userAccount = users.get(i).getAccount();
        	set.add(userAccount);
        }
        if(!set.add(account)) {
            return true;
        } else {
            return false;
        }
    }

    //編集でアカウント重複チェックしない
    public static boolean accountThrough(HttpServletRequest request) {

    	int userId = Integer.parseInt(request.getParameter("user_id"));
        User editUser = new UserService().getUser(userId);

        String editUserAccount = editUser.getAccount();
        String inputAccount = request.getParameter("account");

        if(inputAccount.equals(editUserAccount)) {
            return true;
        } else {
            return false;
        }
    }


}