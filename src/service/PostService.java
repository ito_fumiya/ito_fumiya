package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.UserPost;
import dao.PostDao;

public class PostService {

	//投稿
    public void register(Post Post) {
        Connection connection = null;
        try {
        	System.out.println("PostService register() in");
            connection = getConnection();

            PostDao PostDao = new PostDao();
            PostDao.insert(connection, Post);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //投稿取得
    public List<UserPost> getPost(String start, String end, String category) {

    	String defaultStart = "2018-01-01";
    	Date defaultEnd = new Date();
    	SimpleDateFormat d1 = new SimpleDateFormat("yyyy-MM-dd");

        Connection connection = null;
        try {
            connection = getConnection();
            PostDao PostDao = new PostDao();


        	if(start == null || start == "") {
            	start = defaultStart;
            }
            if(end == null || end == "") {
            	end = d1.format(defaultEnd);
            }
            if(category == null || category.isEmpty()) {
            	category = null;
            }

           	List<UserPost> ret = PostDao.getUserPosts(connection, start, end, category);


            commit(connection);
            System.out.println("PostService getPost()");
            return ret;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //投稿削除
    public void deletePost(int postId) {
        Connection connection = null;
        try {
        	System.out.println("PostService deletePost() in");
            connection = getConnection();

            PostDao PostDao = new PostDao();
            PostDao.delete(connection, postId);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //投稿のバリデーション
    public static boolean isPostValid(HttpServletRequest request, List<String> Messages) {

    	String Title = request.getParameter("title");
        String Text = request.getParameter("text");
        String Category = request.getParameter("category");

        if(StringUtils.isBlank(Category)) {
        	Messages.add("カテゴリを入力してください");
        }else if(10 < Category.length()) {
        	Messages.add("カテゴリは10文字以下で入力してください");
        }

        if(StringUtils.isBlank(Title)) {
        	Messages.add("タイトルを入力してください");
        }else if(30 < Title.length()) {
        	Messages.add("タイトルは30文字以下で入力してください");
        }

        if (StringUtils.isBlank(Text)) {
            Messages.add("本文を入力してください");
        }else if (1000 < Text.length()) {
            Messages.add("本文は1000文字以下で入力してください");
        }

        if (Messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }


}