package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	//ユーザ登録
    public void insert(Connection connection, User user) {
    	 System.out.println("User Dao in");
        PreparedStatement ps = null;
        try {
        	System.out.println("UserDao insert in");
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("name");
            sql.append(", account");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", states");
            sql.append(", password");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(") VALUES (");
            sql.append("?"); // name
            sql.append(", ?"); // account
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", ?"); // states
            sql.append(", ?"); // password
            sql.append(", CURRENT_TIMESTAMP"); // created_at
            sql.append(", CURRENT_TIMESTAMP"); // updated_at
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getName());
            ps.setString(2, user.getAccount());
            ps.setInt(3, user.getBranchId());
            ps.setInt(4, user.getPositionId());
            ps.setInt(5, user.getStates());
            ps.setString(6, user.getPassword());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ログイン
    public User getUser(Connection connection, String account,
            String password) {
        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            System.out.println("UserDao getuser() ");

            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {
        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String account = rs.getString("account");
                int branchId = rs.getInt("branch_id");
                int positionId = rs.getInt("position_id");
                int states = rs.getInt("states");
                String password = rs.getString("password");
                Timestamp createdAt = rs.getTimestamp("created_at");
                Timestamp updatedAt = rs.getTimestamp("updated_at");


                User user = new User();
                user.setId(id);
                user.setName(name);
                user.setAccount(account);
                user.setBranchId(branchId);
                user.setPositionId(positionId);
                user.setStates(states);
                user.setPassword(password);
                user.setCreatedAt(createdAt);
                user.setUpdatedAt(updatedAt);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    //ユーザ編集
    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
        	System.out.println("UserDao UserEdit getUser ");

            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  account = ?");
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            if(StringUtils.isBlank(user.getPassword()) == false) {
            sql.append(", password = ?");
            }
            sql.append(", updated_at = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setInt(3, user.getBranchId());
            ps.setInt(4, user.getPositionId());
            if(StringUtils.isBlank(user.getPassword()) == false) {
	            ps.setString(5, user.getPassword());
	            ps.setInt(6, user.getId());
            } else {
            	ps.setInt(5, user.getId());
            }


            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ユーザ一覧取得のgetUser
    public List<User> getUserIndex(Connection connection) {
        PreparedStatement ps = null;
        try {

        	System.out.println("UserDao getUserIndex ");

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.name as name, ");
            sql.append("users.account as account, ");
            sql.append("users.branch_id as branch_id, ");
            sql.append("users.position_id as position_id, ");
            sql.append("branches.name as branch_name, ");
            sql.append("positions.name as position_name, ");
            sql.append("users.states as states, ");
            sql.append("users.password as password, ");
            sql.append("users.created_at as created_at, ");
            sql.append("users.updated_at as updated_at ");
            sql.append("FROM users INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id ");
            sql.append("ORDER BY branch_id ASC ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = userList(rs);

            return ret;
        } catch (SQLException e) {
        	
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> userList(ResultSet rs) throws SQLException {
        List<User> ret = new ArrayList<User>();
        try {
        	System.out.println("UserDao userList");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String account = rs.getString("account");
                int branchId = rs.getInt("branch_id");
                int positionId = rs.getInt("position_id");
                String branchName = rs.getString("branch_name");
                String positionName = rs.getString("position_name");
                int states = rs.getInt("states");
                String password = rs.getString("password");
                Timestamp createdAt = rs.getTimestamp("created_at");
                Timestamp updatedAt = rs.getTimestamp("updated_at");


                User user = new User();
                user.setId(id);
                user.setName(name);
                user.setAccount(account);
                user.setBranchId(branchId);
                user.setPositionId(positionId);
                user.setBranchName(branchName);
                user.setPositionName(positionName);
                user.setStates(states);
                user.setPassword(password);
                user.setCreatedAt(createdAt);
                user.setUpdatedAt(updatedAt);

                ret.add(user);
            }

            return ret;
        } finally {
            close(rs);
        }
    }

  //ステータス変更
    public void updateStates(Connection connection, int id, int states) {

        PreparedStatement ps = null;
        try {
        	System.out.println("UserDao updateStates ");

            String sql = "UPDATE users SET states = ? WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, states);
            ps.setInt(2, id);


            int count = ps.executeUpdate();

            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
