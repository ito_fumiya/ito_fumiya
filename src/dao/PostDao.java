package dao;

import static  utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Post;
import beans.UserPost;
import exception.SQLRuntimeException;

public class PostDao {

	//投稿
    public void insert(Connection connection, Post post) {
    	 System.out.println("postdao insert ");

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO  posts ( ");
            sql.append(" title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", user_id");
            sql.append(", created_at");
            sql.append(") VALUES (");
            sql.append(" ?"); // title
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date

            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, post.getTitle());
            ps.setString(2, post.getText());
            ps.setString(3, post.getCategory());
            ps.setInt(4, post.getUserId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //投稿削除
    public void delete(Connection connection, int postId) {
   	 System.out.println("Post Dao delete in");

       PreparedStatement ps = null;
       try {
           StringBuilder sql = new StringBuilder();
           sql.append("DELETE FROM posts WHERE id = ? ");

           ps = connection.prepareStatement(sql.toString());

           ps.setInt(1, postId);

           ps.executeUpdate();

       } catch (SQLException e) {
           throw new SQLRuntimeException(e);
       } finally {
           close(ps);
       }
    }

    //投稿表示
    public List<UserPost> getUserPosts(Connection connection, String start, String end, String category) {
        PreparedStatement ps = null;


        try {
        	System.out.println("UserPostDao getUserPosts in");

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.title as title, ");
            sql.append("posts.text as text, ");
            sql.append("posts.category as category, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_at as created_at ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("WHERE posts.created_at ");
            sql.append(">= ? ");
            sql.append("AND posts.created_at ");
            sql.append("<= ? ");
            if(category != null) {
            	sql.append("AND posts.category LIKE ? ");
            }
            sql.append("ORDER BY created_at DESC");


            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, start);
            ps.setString(2, end + " 23:59:59");
            if(category != null ) {
            	ps.setString(3, "%" + category +"%");
            }

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);

            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserPostList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                Timestamp createdAt = rs.getTimestamp("created_at");

                UserPost Post = new UserPost();
                Post.setName(name);
                Post.setId(id);
                Post.setUserId(userId);
                Post.setText(text);
                Post.setTitle(title);
                Post.setCategory(category);
                Post.setCreatedAt(createdAt);

                ret.add(Post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
