package dao;

import static  utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Position;
import exception.SQLRuntimeException;

public class PositionDao {

	  public List<Position> getPosition(Connection connection) {
	        PreparedStatement ps = null;
	        try {

	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT * FROM positions");

	            ps = connection.prepareStatement(sql.toString());

	            ResultSet rs = ps.executeQuery();
	            List<Position> ret = positionList(rs);
	            System.out.println("PositionDao getPosition out");
	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	  private List<Position> positionList(ResultSet rs) throws SQLException {
	        List<Position> ret = new ArrayList<Position>();
	        try {
	        	System.out.println("PositionDao PositionList");
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String name = rs.getString("name");

	                Position position = new Position();
	                position.setId(id);
	                position.setName(name);

	                ret.add(position);
	            }
	            System.out.println("PositionDao PositionList out");
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }
}
