package dao;

import static  utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import exception.SQLRuntimeException;

public class CommentDao {

	//コメント投稿
    public void insert(Connection connection, Comment comment) {
    	 System.out.println("commentdao insert in");

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO  comments ( ");
            sql.append(" text");
            sql.append(", user_id");
            sql.append(", post_id");
            sql.append(", created_at");
            sql.append(") VALUES (");
            sql.append(" ?"); // text
            sql.append(", ?"); // user_id
            sql.append(", ?"); // post_id
            sql.append(", CURRENT_TIMESTAMP"); // created_at
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getText());
            ps.setInt(2, comment.getUserId());
            ps.setInt(3, comment.getPostId());
            ps.executeUpdate();
            System.out.println("postdao insert ok");
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //コメント削除
    public void delete(Connection connection, int commentId) {
   	 System.out.println("CommentDao delete in");

       PreparedStatement ps = null;
       try {
           StringBuilder sql = new StringBuilder();
           sql.append("DELETE FROM comments WHERE id = ? ");


           ps = connection.prepareStatement(sql.toString());

           ps.setInt(1, commentId);
           ps.executeUpdate();
           System.out.println("Comment Dao delete ok");
       } catch (SQLException e) {
           throw new SQLRuntimeException(e);
       } finally {
           close(ps);
       }
   }

    //コメント表示
    public List<UserComment> getUserComments(Connection connection) {
        PreparedStatement ps = null;
        try {
        	System.out.println("UserCommentDao getUserComments ");
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("Comments.id as id, ");
            sql.append("Comments.text as text, ");
            sql.append("Comments.user_id as user_id, ");
            sql.append("Comments.post_id as post_id, ");
            sql.append("users.name as name, ");
            sql.append("Comments.created_at as created_at ");
            sql.append("FROM Comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON Comments.user_id = users.id ");
            sql.append("ORDER BY created_at ASC");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            System.out.println("UserCommentDao getUserComments ");
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String text = rs.getString("text");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                int postId = rs.getInt("post_id");
                Timestamp createdAt = rs.getTimestamp("created_at");

                UserComment Comment = new UserComment();
                Comment.setName(name);
                Comment.setId(id);
                Comment.setUserId(userId);
                Comment.setPostId(postId);
                Comment.setText(text);

                Comment.setCreatedAt(createdAt);

                ret.add(Comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


}