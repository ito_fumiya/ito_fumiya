package beans;


import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String text;
    private int userId;;
    private int postId;
    private Date createdAt;

    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }

    public int getUserId() {
    	return userId;
    }
    public void setUserId(int userId) {
    	this.userId = userId;
    }

    public int getPostId() {
    	return postId;
    }
    public void setPostId(int postId) {
    	this.postId = postId;
    }

    public Date getCreatedAt() {
    	return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
    	this.createdAt = createdAt;
    }

}