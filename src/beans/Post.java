package beans;

import java.io.Serializable;
import java.util.Date;

public class Post implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String title;
    private String text;
    private String category;
    private int userId;
    private Date createdAt;

    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    public String getTitle() {
    	return title;
    }
    public void setTitle(String title) {
    	this.title = title;
    }

    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }

    public String getCategory() {
    	return category;
    }
    public void setCategory(String category) {
    	this.category = category;
    }

    public int getUserId() {
    	return userId;
    }
    public void setUserId(int userId) {
    	this.userId = userId;
    }

    public Date getCreatedAt() {
    	return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
    	this.createdAt = createdAt;
    }

}