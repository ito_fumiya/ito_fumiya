package beans;

import java.io.Serializable;
import java.util.Date;

public class UserPost implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String text;
    private String title;
    private String category;
    private String name;
    private int userId;
    private Date createdAt;


    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }

    public String getTitle() {
    	return title;
    }
    public void setTitle(String title) {
    	this.title = title;
    }

    public String getCategory() {
    	return category;
    }
    public void setCategory(String category) {
    	this.category = category;
    }

    public String getName() {
    	return name;
    }
    public void setName(String name) {
    	this.name = name;
    }

    public int getUserId() {
    	return userId;
    }
    public void setUserId(int userId) {
    	this.userId = userId;
    }

    public Date getCreatedAt() {
    	return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
	   this.createdAt = createdAt;
    }


}