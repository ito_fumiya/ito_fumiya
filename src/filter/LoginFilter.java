package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		System.out.println("loginFilter");

		HttpSession session = ((HttpServletRequest)request).getSession();
		User user = (User)session.getAttribute("loginUser");

		String servletName = ((HttpServletRequest) request).getServletPath();

		if(user == null && !servletName.equals("/login") && !servletName.equals("/style.css")
				&& !servletName.equals("/js/jquery-3.3.1.min.js")) {

			String message = "ログインしてください";

	    	session.setAttribute("errorMessages", message);

			((HttpServletResponse)response).sendRedirect("./login");
		}else {
			chain.doFilter(request, response); // サーブレットを実行
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
