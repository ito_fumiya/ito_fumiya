package filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebFilter("/setting")
public class UrlFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		System.out.println("UrlFilter");

		HttpSession session = ((HttpServletRequest)request).getSession();

		if(isUrlValid(request) == false ) {

			String message = "入力された値が不正です";
	    	session.setAttribute("errorMessages", message);

			((HttpServletResponse)response).sendRedirect("./management");
		}else {
			chain.doFilter(request, response); // サーブレットを実行s
		}
	}

	 //UserSettingのURLでの存在しないIDチェック
		public static boolean isUrlValid(ServletRequest request) {
			System.out.println(" UserService isUrlValid");

	        String existId = request.getParameter("user_id");

	        List<User> users = new UserService().getUserIndex();
	        Set<String> set = new HashSet<>();

	        //userの数だけIDをハッシュセットに追加
	        for(int i=0; i <users.size() ; i++){
	        	int userId = users.get(i).getId();
	        	set.add(String.valueOf(userId));
	        }
	        if(!set.add(existId)) {
	            return true;
	        } else {
	            return false;
	        }
		}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
